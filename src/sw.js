const CACHE_NAME = "what-to-do-v1";

const cacheUrls = [
    "/"
];

self.addEventListener('install', function(ev){
    console.log(ev)
    cache.open(CACHE_NAME)
        .then(function(cache){
            return cache.addAll(cacheUrls);
        })
});

self.addEventListener('activate', function(ev){
    console.log('SW actualizada');
    
})