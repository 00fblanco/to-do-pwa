import { Component, OnInit } from '@angular/core';

import { IList } from './../../structures/lists';
import { ListService } from './../../services/list.service';


@Component({
    selector: 'creator',
    templateUrl: 'list.creator.component.html'
})
export class ListCreatorComponent implements OnInit {
    constructor( private listS: ListService) {}

    public list: IList = { title: ''};
    ngOnInit() {}

    save() {
        this.listS.add(this.list).then(result => {
            this.list.title = '';
        });
    }
}
