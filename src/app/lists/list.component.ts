import { trigger, state, style, animate, transition, query, stagger } from '@angular/animations';

import { TodoService } from './../services/todos.service';
import { Observable } from 'rxjs/Rx';
import { ITodo } from './../structures/todos';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'list',
    templateUrl: 'list.component.html',
    animations: [
        trigger('enterState', [
          transition('* => *', [
            query(':enter', [
              style({ transform: 'translateX(-100%)', opacity: 0}),
              stagger(50, [
                animate(200, style({ transform: 'translateX(0)', opacity: 1 }))
              ])
            ], { optional: true})
          ])
        ])
      ]
})


export class ListComponent implements OnInit {
    public listId: string;
    public todos: Observable<ITodo[]>;

    trackTodoObjects = (id, obj) => obj.id;

    constructor(private route: ActivatedRoute, private todoS: TodoService) {}
    ngOnInit() {
        this.listId = this.route.snapshot.params.id;
        this.todos = this.todoS.getFromList(this.listId);
    }
}
