export interface ITodo {
    id ?: string;
    content: string;
    description ?: string;
    createdAt ?: any;
    status: TStatus;
    deadline ?: any;
}

export enum TStatus {
    Created = 0,
    Completed,
    Failed,
    Expired
}
