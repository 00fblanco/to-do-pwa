import { take } from 'rxjs/operators';
import { Observable } from 'rxjs/Rx';
import { AngularFireAuth } from 'angularfire2/auth';
import { Injectable } from '@angular/core';
import { CanActivate, RouterStateSnapshot, ActivatedRouteSnapshot, Router } from '@angular/router';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
// import 'rxjs/add/operator/take';


@Injectable()
export class AuthGuard implements CanActivate {
    take
    constructor(private afAuth: AngularFireAuth, private router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        // console.log('ASAS', this.afAuth.authState);
        return this.afAuth.authState.take(1).map((user: firebase.User) => {
            return !!user;
        }).do((authenticated: boolean) => {
            if (!authenticated) { this.router.navigate(['/login']); }
        });
    }
}
